﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_in_basement : MonoBehaviour
{
    public bool PlayerInBasement = false;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerInBasement = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        PlayerInBasement = false;
    }

    
}
