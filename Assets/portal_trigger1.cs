﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portal_trigger1 : MonoBehaviour
{
    public GameObject portal1;
    private AkRoomPortal AKPortal1;
    
    
    // Start is called before the first frame update
    void Start()
    {
        AKPortal1 = portal1.GetComponent<AkRoomPortal>();
    }

    // Update is called once per frame
    public void PortalOpen1()
    {
        AKPortal1.Open();
    }
    public void PortalClose1()
    {
        AKPortal1.Close();
    }
}
