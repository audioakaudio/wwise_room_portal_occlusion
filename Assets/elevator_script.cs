﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevator_script : MonoBehaviour
{

    Animator animator;
    bool topdoorOpen = false;
    bool basementdoorOpen = false;
    //bool PlayerInBasement = false;
    bool PlayerInTrigger = false;
    bool elevatorup = true;
    bool PlayBase = false;

    public AK.Wwise.Event openEvent;
    public AK.Wwise.Event closeEvent;
    public AK.Wwise.Event elevatorStart;
    public AK.Wwise.Event elevatorStop;
    public GameObject door;
    public GameObject basement;
    player_in_basement BasementComp;




    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        BasementComp = basement.GetComponent<player_in_basement>();    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerInTrigger = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        PlayerInTrigger = false;
    }

    void ElevatorControl(string direction)
    {
        animator.SetTrigger(direction);
    }

    private void Update()
    {

        PlayBase = BasementComp.PlayerInBasement;


        if (PlayerInTrigger && !topdoorOpen && !PlayBase && elevatorup && Input.GetKeyDown("e"))
        {
            topdoorOpen = true;
            basementdoorOpen = false;
            ElevatorControl("open_top");

        }
        else if (PlayerInTrigger && topdoorOpen && elevatorup && Input.GetKeyDown("0"))
        {
            
            ElevatorControl("down");
            elevatorup = false;
            topdoorOpen = false;
            basementdoorOpen = true;
        }

        else if (PlayerInTrigger && !topdoorOpen && !elevatorup && basementdoorOpen && Input.GetKeyDown("1"))
        {

            ElevatorControl("up");
            elevatorup = true;
            topdoorOpen = true;
            basementdoorOpen = false;
        }

        else if (!PlayerInTrigger && topdoorOpen )
        {

            ElevatorControl("close_top");
            elevatorup = true;

            topdoorOpen = false;
        }

        else if (!PlayerInTrigger && basementdoorOpen)
        {

            ElevatorControl("close_basement");
            elevatorup = false;
            topdoorOpen = false;
            basementdoorOpen = false;

        }

        else if (!basementdoorOpen && PlayBase && Input.GetKeyDown("e"))
        {
            
            basementdoorOpen = true;
            
            ElevatorControl("open_basement");

        }

    }
    void playElevatorOpen()
    {
        openEvent.Post(door);
    }
    void playElevatorClose()
    {
        closeEvent.Post(door);
    }

    void playElevatorStart()
    {
        elevatorStart.Post(door);
    }
    void playElevatorStop()
    {
        elevatorStop.Post(door);
    }

    private void OnGUI()
    {
        GUILayout.Space(100);
        GUILayout.Label("topdoorOpen: " + topdoorOpen);
        GUILayout.Label("basementdoorOpen: " + basementdoorOpen);
        GUILayout.Label("PlayerInTrigger: " + PlayerInTrigger);
        GUILayout.Label("elevatorup: " + elevatorup);
        GUILayout.Label("PlayerInBasement: " + PlayBase);
    }
}
